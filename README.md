# Start

```
git clone https://gitlab.com/AndrewKruglik/impairedspeechrecognition.git
cd impairedspeechrecognition/
python -m venv venv
venv/Scripts/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```

To add secret_access_key in .dvc/config

```
dvc pull
python src/data/make_dataset.py "data\raw\Разметка текста" "data\interim\dataset.csv"
```


impairedspeechrecognition
==============================

A short description of the project.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>


--------



# ТЕХНИЧЕСКОЕ ЗАДАНИЕ <br> по проекту <br> «Разработка системы распознавания речи пользователей голосовых чатов с дизартрией»

## 1. Общие сведения

### 1.1 Наименование проекта
НИОКР-проект «Разработка системы распознавания речи пользователей голосовых чатов с дизартрией».

### 1.2 Список принятых сокращений
|Термин|Расшифровка|
|------|-----------|
|Заказчик|ООО «ИТ безграничных возможностей»|
|ОКР|Опытно-конструкторские разработки|
|НИР|Научно-исследовательские работы|
|WER|Word Error Rate|
|CER|Character Error Rate|

## 2 Назначения, цели и сроки проекта
### 2.1 Назначение проекта
Назначение проекта заключается в создании модуля распознавания речи с нарушениями для пользователей чат-бота. Данный модуль предназначен для преобразования потока аудиосигнала с речи (голоса) с нарушениями (с дизартрией) в текст.

### 2.2 Цели проекта

> Реализовать и протестировать алгоритмы, позволяющие распознать речь с нарушениями (с дизартрией)
1. Работоспособность решения подтверждена минимум десятью пользователями с заметным нарушением речи (дизартрией); 
2. Качество алгоритма (модели) оценивается:
   1. С привлечением экспертного мнения;
   2. Путем расчета метрики WER между выводом алгоритма и ground-truth фразой (см ниже): WER < 0.3.
   3. Путем расчета метрики CER между выводом алгоритма и ground-truth фразой (см ниже): CER < 0.3.

Для того, чтобы рассчитать метрику WER, необходимо сумму количества замен s, удалений d и вставок i разделить на количество сказанных слов n: 
<br>WER =(s+d+i)/n. <br>
Замена происходит при замене слова (например, «стул» распознаётся как «стол»). Вставка — это наличие слова в выходной последовательности модели, которое не было сказано. Удаление происходит, когда слово полностью отсутствует в выходной последовательности модели.
<br>Для того, чтобы рассчитать метрику WER, необходимо сумму минимального количества вставок (i), замен (s) и удалений (d) символов, необходимых для получения результата Ground Truth (целевого значения), разделить на  общее количество символов (n), включая пробелы. Формула для расчета CER выглядит следующим образом: 
<br>CER=(i+s+d)/n.<br>

### 2.3 Задачи проекта

* Сбор и подготовка данных - оцифрованных записей голоса (формата ogg) пользователей, владеющими русским языком, с нарушением речи и информации о виде нарушения;
* Выбор звукозаписей с видом нарушения речи - дизартрия;
* Распознавание речи звукозаписи с помощью существующих алгоритмов (Google speech-to-text, Yandex SpeechKit);
* Разметка фрагментов звукозаписи речи, которые существующие алгоритмы не смогли распознать.
* Разработка критериев оценки алгоритма участка сегмента записи с нарушением речи, алгоритма распознавания речи на таких участках
* Обзор и сравнительный анализ существующих opensource алгоритмов преобразования по разработанным критериям;
* Анализ наработок (ФТТ, предварительное архитектурное видение, предварительная программная реализация) по результатам НИР-этапа проекта;
* Реализация и адаптация наилучшего из существующих алгоритмов, либо разработка собственного, в случае отсутствия подходящих по критериям;
* Тестирование алгоритма на данных Заказчика, до этого не задействованных в создании алгоритма;
* Подготовка рекомендаций по дальнейшей доработке алгоритма;
* Подготовка итоговых материалов по проекту (отчет, презентация).
* Определение дальнейших шагов развития проекта (конверсия и бэклог для цифрового продукта).

### 2.4 Результаты проекта
Результаты проекта:
1. Исходный программный код
2. Программный продукт в виде библиотеки python с сопутствующим ему руководством программиста
3. Итоговый отчет, включающий в себя:
   * Результаты анализа и предобработки данных;
   * Метрики качества работы алгоритмов и необходимых эвристик;
   * Сравнение алгоритмов и описание наилучшего с точки зрения метрик решения;
   * Результаты тестирования на валидационной выборке данных.
   * Границы применимости алгоритма;
   * Рекомендации по доработке алгоритма;
   * Бэклог на следующий этап работ;
4. Демо-ролик работы алгоритма; 
5. Заключения экспертов от бизнеса (логопедов);
6. Презентация с выводами по итогу проекта, рекомендациями и заключением по дальнейшему развитию проекта.

### 2.5 Требования к промежуточной отчетности
В рамках проекта осуществляется взаимодействие между представителями _Заказчика_ и _Исполнителя_ путем оговоренной еженедельной конференцсвязи для демонстрации результатов еженедельного спринта в формате презентации. Вопросы, возникающие в ходе реализации проекта, уточняются между указанными лицами по мере возникновения.

### 2.6 Сроки проекта
Результаты по проекту необходимо передать и согласовать с Заказчиком в срок до **15.08.2023 г.**
## 3 Требования к документированию
По результатам реализации НИОКР-проекта необходимо предоставить:
* Отчет о проведении НИОКР, в формате _.doсx_;
* Руководство программиста;
* Демо-ролик, в формате _.avi_;
* Презентация об итогах реализации НИОКР-проекта, в формате _.pptx_.










