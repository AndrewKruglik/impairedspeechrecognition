'''Module for make dataset'''

import os
import re
import json
import logging

from pathlib import Path
from collections import defaultdict

import pandas as pd
import click

from dotenv import find_dotenv, load_dotenv


@click.command()
@click.argument("input_folder_path", type=click.Path(exists=True))
@click.argument("output_file_path", type=click.Path())
def main(input_folder_path=None, output_file_path=None):
    """Read markups (JSON files) for each text, union these markups items
     in output file (CSV) with info about audio file.

    Parameters
    ----------
    input_folder_path : str
        Path of input folder with JSON files of markups and audio files
    output_file_path : str
        Path of dataset file (CSV) with info about each markup item
    ----------

    Raises
    ----------
    ValueError
        If file format is unknown for script
    ----------
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    file_paths = []
    texts_dict = defaultdict(dict)
    markups = []

    # regexp for extracting number of text
    regexp_text_num = re.compile(r"Текст (\d+)")
    # file formats that can be processed
    known_file_formats = ["wav", "json"]
    # string that is contained will be exclude file
    exclude_strings = ["Микрофон смартфона"]

    # iterate over all folders in input folder
    for path, _, file_names in os.walk(input_folder_path):
        for file_name in file_names:
            file_format = file_name.split(".")[-1]  # get format of file
            exclude_string_exist = any(s in file_name for s in exclude_strings)
            if file_format in known_file_formats and \
                    exclude_string_exist is False:
                # get full path of file
                file_path = os.path.join(path, file_name)
                # save full path of file in list
                file_paths.append(file_path)

    for file_path in file_paths:
        # searching number of text
        search_num = regexp_text_num.search(file_path)
        if search_num is not None:
            text_num = search_num.group(1)  # extracting number of text
            file_format = file_path.split(".")[-1]  # get format of file

            if file_format == "wav":
                file_type = "audio_path"
            elif file_format == "json":
                file_type = "markup_path"
            else:
                raise ValueError("file_format is unknown")

            # save path of file by number of text
            texts_dict[text_num][file_type] = file_path

    for i, text in texts_dict.items():
        # read markup file (JSON)
        with open(text["markup_path"], "r", encoding="utf-8") as markup_file:
            text_markup_data = json.load(markup_file)

        # get all token (word) markups (time intervals)
        markup_items = text_markup_data["contains"][0]["first"]["items"]
        for item in markup_items:
            # get time interval from item markup
            time_interval = item["target"]["id"].split("#t=")[-1]
            # get start and end time of interval
            start_time, end_time = time_interval.split(",")
            markups.append(
                {
                    "text_number": i,
                    "audio_path": text["markup_path"],
                    "token": item["body"]["value"],  # word or token
                    "time_start": float(start_time),
                    "time_end": float(end_time),
                }
            )

    # save all markups to csv file
    df_dataset = pd.DataFrame(markups)
    df_dataset.to_csv(output_file_path)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
