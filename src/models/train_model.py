import os
import mlflow
from mlflow import log_artifacts, log_param, log_metric
from random import random, randint

# os.environ['AWS_S3_BUCKET_NAME'] = 'datas3'
mlflow.set_tracking_uri('http://130.193.36.214:5000')
mlflow.set_experiment('mlflow_test2')

if __name__ == '__main__':
    print(mlflow.get_artifact_uri())

    log_param('param1', randint(0, 10))

    log_metric('metric1', random())
    log_metric('metric2', random() + 10)
    log_metric('metric3', random() + 20)

    log_artifacts('../../data/interim')

